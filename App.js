import React, { useState, useMemo, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Dimensions, StatusBar } from 'react-native';
import MapView, { Polyline, Marker } from 'react-native-maps';
import pick from 'lodash/pick'
import haversine from 'haversine'
import * as Location from 'expo-location'
import * as TaskManager from 'expo-task-manager';

const LOCATION_TRACKING = 'location-tracking';
const { width, height } = Dimensions.get('window')
export default function App() {
  const [routeCoord, setRouteCoord] = useState([])
  const [distanceTravelled, setDistance] = useState(0)
  const [prevLatLng, setPrevLatlng] = useState({})


  const startLocationTracking = async () => {
    await Location.startLocationUpdatesAsync(LOCATION_TRACKING, {
      accuracy: Location.Accuracy.Highest,
      timeInterval: 5000,
      distanceInterval: 0,
    });
    const hasStarted = await Location.hasStartedLocationUpdatesAsync(
      LOCATION_TRACKING
    );
    console.log('tracking started?', hasStarted);
  };


  TaskManager.defineTask(LOCATION_TRACKING, async ({ data, error }) => {
    if (error) {
      console.log('LOCATION_TRACKING task ERROR:', error);
      return;
    }
    if (data) {
      const { locations } = data;
      let lat = locations[0].coords.latitude;
      let long = locations[0].coords.longitude;
      let newLatLng = {
        latitude: lat,
        longitude:long
      }
      console.log("latlng", routeCoord.length)
      setDistance(distanceTravelled + calcDistance(newLatLng))
      setRouteCoord(routeCoord.concat(newLatLng))
      setPrevLatlng(newLatLng)
    }
  });


  useMemo(async () => {
    StatusBar.setBarStyle('light-content')
    const res = await Location.requestPermissionsAsync()
    if (res.status !== 'granted') {
      console.log('Permission to access location was denied');
    } else {
      console.log('Permission to access location granted');
    }
    startLocationTracking()
  }, [])


  const calcDistance = (newLatLng) => {
    let result = (haversine(prevLatLng, newLatLng) || 0)
    console.log('calcDistance', result)
    return result
  }
  return (
    <View style={styles.container}>
      {/* {
        routeCoord.length !== 0 ? */}
        <MapView
          style={{ width, height }}
          mapType="standard"
          showsUserLocation={true}
          followUserLocation={true}
          zoomControlEnabled={true}
          maxZoomLevel={20}
          minZoomLevel={13}
          region={routeCoord[routeCoord.length-1]}
        >
          <Polyline
            coordinates={routeCoord}
            strokeColor='blue'
            lineWidth={10}
            strokeWidth={10}
          />
        </MapView>
        {/* :
        false
      } */}
      <View style={styles.navBar}><Text style={styles.navBarText}>Run tracking</Text></View>
      <View style={styles.bottomBar}>
        <View style={styles.bottomBarGroup}>
          <Text style={styles.bottomBarHeader}>DISTANCE</Text>
          <Text style={styles.bottomBarContent}>{parseFloat(distanceTravelled).toFixed(2)} km</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  navBar: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    height: 64,
    width: width,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  navBarText: {
    color: '#19B5FE',
    fontSize: 16,
    fontWeight: "700",
    textAlign: 'center',
    paddingTop: 30
  },
  map: {
    flex: 0.7,
    width: width,
    height: height
  },
  bottomBar: {
    position: 'absolute',
    height: 100,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.7)',
    width: width,
    padding: 20,
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  bottomBarGroup: {
    flex: 1
  },
  bottomBarHeader: {
    color: '#fff',
    fontWeight: "400",
    textAlign: 'center'
  },
  bottomBarContent: {
    color: '#fff',
    fontWeight: "700",
    fontSize: 18,
    marginTop: 10,
    color: '#19B5FE',
    textAlign: 'center'
  },
});
